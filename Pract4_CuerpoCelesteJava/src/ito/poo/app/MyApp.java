package ito.poo.app;

import Pract4_CuerpoCelesteUML.CuerpoCeleste;
import Pract4_CuerpoCelesteUML.Ubicacion;

public class MyApp {

	public static void main(String[] args) {
		
		CuerpoCeleste cuerpo;
		cuerpo=new CuerpoCeleste("Estrella", null, "solido");
	    System.out.println(cuerpo);
	    
	    Ubicacion ubicacion;
	    ubicacion=new Ubicacion(94.05f, 239.47f, "3 meses", 940.10f);
	    System.out.println(ubicacion);
	}
}
